import requests
from datetime import datetime
from bs4 import BeautifulSoup
import csv

response = requests.get("https://www.gloshospitals.nhs.uk/")
html = BeautifulSoup(response.text, "html.parser")

# Get the waiting time widgets
wait_stats = html.find_all(attrs={"class": "waiting-times__item"})

class HospitalWait:
    def __init__(self, wait_html):
        # each waiting times element appears to have one heading
        self.name = wait_html.find_all(attrs={"class": "waiting-times__item-heading"})[0].text.strip()
        # but 2 statistics
        stat_sets = wait_html.find_all(attrs={"class": "waiting-times__icon-set"})
        self.stats = []
        for stat in stat_sets:
            # There should be only one of each of these in a single "stat"
            self.stats.append({
                "label": stat.find_all(attrs={"class": "waiting-times__meta"})[0].text.strip(),
                "value": int(stat.find_all(attrs={"class": "waiting-times__stat"})[0].text.strip()) # might as well insist on a number mightn't we
            })
        # The datetime string doesn't play nice with the datetime library (it ALMOST does)
        # We see something like Last updated: 24 Sep 2018, 12:29 p.m. but can process Last updated: 24 Sep 2018, 12:29 pm
        timestring = wait_html.find_all(attrs={"class": "waiting-times__update-info"})[0].text.strip().replace(" p.m.", " pm").replace(" a.m.", " am")
        # once this is in a datetime format we can manipulate it if we want (we don't write now, but hey ho)
        self.updated = datetime.strptime(timestring, "Last updated: %d %b %Y, %I:%M %p")

# Build some hospital data
hospitals = []
for wait_stat in wait_stats:
    hs = HospitalWait(wait_stat)
    hospitals.append(hs)
    

# Dick around with the data in suitable ways


mainlist = [] #simple values array
timestamp = [] #timestamp in one user friendly, easy to use variable
for hospital in hospitals:
    print(hospital.name)   
    for stat in hospital.stats:
        print("- %d %s" % (stat["value"], stat["label"]))
        mainlist.append(stat["value"]) #compile values into one array to make it easy

    # This just rearranges the datetime as an example
    print("Up to date at: %s" % datetime.strftime(hospital.updated, "%Y/%m/%d %H:%M"))
    timestamp = ( datetime.strftime(hospital.updated, "%Y/%m/%d %H:%M"))
    print("\n")

print(timestamp)  
print(*mainlist, sep = ", ") #print the values for debuggery
with open("waittimes.csv", "a", newline='') as csvFile: #open csv file in append mode, suppress extra line break at end
            csvFileWriter = csv.writer(csvFile)
            csvFileWriter.writerow([timestamp,*mainlist])#write whole shebang out to a new line on the csv file
           

