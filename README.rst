What this is
============
A really really basic scraper to pull the wait times off the Gloucester Hospitals website

Pre-requisites
==============

* python3
* python virtualenv installed (recommend doing this rather than polluting your system-wide python with 1000s of libraries)

Installation (Windows systems using git-bash)
=============================================

    virtualenv --no-site-packages ./v
    ./v/Scripts/pip install -r requirements.txt


Installation (Unix-type systems)
================================

    virtualenv --no-site-packages ./v
    ./v/bin/pip install -r requirements.txt

Running (Windows systems using git-bash)
========================================

    ./v/Scripts/python scrayande.py


Running (Unix-type systems)
===========================

    ./v/bin/python scrayande.py

Note
====
You could totally write a shell script to detect your OS and run the right commands but I couldn't be bothered / thought I should do some actual work now


